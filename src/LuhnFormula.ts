"use strict";

function input_validation():number
{
	const regex = new RegExp("[^0-9\\s]+");

	if (process.argv.length <= 2 || 
		process.argv[2].search(regex) != -1 ||
		process.argv[2].length <= 1)
		return (42);
	return (0);
}

function convert_to_int(str:string):number[]
{
	let numbs:number[] = [0];

	for (let i = 0; i < str.length; i++)
		numbs[i] = parseInt(str[i], 10);
	return (numbs);
}

function is_valid_credit_num():number
{
	let input	:string = process.argv[2].replace(/ +/g, '');
	let card_num:number[] = convert_to_int(input);
	let sum		:number = 0;

	for (let i = 0; i < card_num.length; i++)
	{
		if ((i + 1) % 2 <= 0)
		{
			if (9 < card_num[i] * 2)
				card_num[i] = card_num[i] * 2 - 9;
			else
				card_num[i] = card_num[i] * 2;
		}
		sum += card_num[i];
	}
	return ((sum % 10 == 0) ? 0 : 42);
}

function is_luhn_valid()
{
	return (input_validation() || is_valid_credit_num());
}

if (is_luhn_valid())
	process.exit(42);